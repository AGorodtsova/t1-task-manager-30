package ru.t1.gorodtsova.tm.exception.entity;

public final class ModelNotFoundException extends AbstractEntityNotFoundException {

    public ModelNotFoundException() {
        super("Error! Object not found...");
    }

}
