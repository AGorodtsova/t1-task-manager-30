package ru.t1.gorodtsova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.gorodtsova.tm.enumerated.Status;
import ru.t1.gorodtsova.tm.util.TerminalUtil;

public final class ProjectStartByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private final String DESCRIPTION = "Start project by index";

    @NotNull
    private final String NAME = "project-start-by-index";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        getProjectService().changeProjectStatusByIndex(getUserId(), index, Status.IN_PROGRESS);
    }

}
